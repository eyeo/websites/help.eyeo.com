# help.eyeo.com

Source code and content of [help.eyeo.com](https://help.eyeo.com).

## Getting started

**1. Install [CMS](https://github.com/adblockplus/cms)**

This static site is generated using the Adblock Plus CMS. For more information 
and usage instructions see [CMS documentation](https://github.com/adblockplus/cms/blob/master/README.md#content-structure).

**2. Install [Gulp.js](https://gulpjs.com/) globally**

Gulp is required to build the static assets from the source files.

```
npm install gulp-cli -g
```

**3. Install dependencies**

```
npm install
```

**4. Build static assets**

```
gulp
```

**5. Run the test server**

Start the cms test server

```
python path/to/cms/runserver.py
```

## Create previews

There are two ways to create previews in Gitlab.

### 1. Update the origin repository preview

The origin eyeo/websites/help.eyeo.com repository preview will be updated automatically each commit.

One can also manually [run a pipeline](https://gitlab.com/eyeo/websites/help.eyeo.com/pipelines) to update the origin repository preview using a specific branch or commit.

### 2. Create a fork

A new preview, like the origin preview, is created for forks of eyeo/websites/help.eyeo.com. e.g.

- Origin repository: https://gitlab.com/eyeo/websites/help.eyeo.com
- Origin preview https://eyeo.gitlab.io/websites/help.eyeo.com

- User fork: https://gitlab.com/${user-name}/${fork-name}
- User preview: https://${user-name}.gitlab.io/${fork-name}

## License

- Source code: [GPL-3.0](https://www.gnu.org/licenses/gpl.html)
- Content: [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/)

## Legal guidelines

- [Legal guidelines to follow when adding an article to the help center](https://gitlab.com/eyeo/websites/websites-module/blob/master/help.eyeo.com/legal-guidelines.md)
