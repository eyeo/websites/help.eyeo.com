<!DOCTYPE html>
<!--
 This file is part of help.eyeo.com.
 Copyright (C) 2017-present eyeo GmbH

 help.eyeo.com is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 help.eyeo.com is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with help.eyeo.com.  If not, see <http://www.gnu.org/licenses/>.
-->
<html
  lang="{{ locale }}"
  dir="{{ "rtl" if config.has_option("rtl", locale) else "ltr" }}"
  class="no-js">
<head>
  
  <? include meta/standard ?>

  <!--[if IE 8]>
    <script src="/js/vendor/ie8.min.js"></script>
  <![endif]-->

  <link rel="stylesheet" href="/dist/css/main.min.css">

  <!--[if lt IE 9]>
    <script src="/js/vendor/html5shiv.min.js"></script>
    <script src="/js/vendor/respond.min.js"></script>
  <![endif]-->

  <!--[if lt IE 10]>
    <script src="/js/vendor/classList.min.js"></script>
    <script src="/js/vendor/element-closest.min.js"></script>
  <![endif]-->

  {% block head %}
    {{ head | safe }}
  {% endblock %}
</head>
<body>
  <? include layout/header ?>
  <div class="outer">
  {% block body %}
    {{ body | safe }}
  {% endblock %}
  </div>
  {% block footer %}
    {{ footer | safe }}
  {% endblock %}
  <? include layout/footer ?>
  {% block scripts %}
    {{ scripts | safe }}
  {% endblock %}
  <script src="/dist/js/main.min.js"></script>
</body>
<html>
