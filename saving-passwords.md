title=Saving passwords
description=How to save passwords to Adblock Browser for Android.
template=article
product_id=abb
category=Customization & Settings

You may find it convenient to save your passwords to Adblock Browser so that you don't have to enter them every time you visit your favorite sites.

<section class="platform-android" markdown="1">

1. Open the Adblock Browser app.
2. Tap the **Android menu icon** and select **Settings**.
3. Tap **Passwords**.
<br>By default, password feature is turned on. If you want don't want Adblock Browser to save your passwords, toggle the feature off.
</section>