title=Selecting a search engine
description=Customize Adblock Browser by choosing from a number of search engines.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

We want to put users in charge of their browsing experience so there are variety of customizations available. Start by choosing your preferred search engine using these instructions.

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Search engine**.
4. Select the search engine that you want to use.

![Select a search engine for Adblock Browser](/src/img/gif/abb-search-engine.gif)
