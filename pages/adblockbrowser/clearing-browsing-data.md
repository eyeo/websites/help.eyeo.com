title=Clear your ABB browsing history, cache, and website data
description=Clear your Adblock Browser browsing history, downloads, form and search history, cookies and active logins, saved passwords, cache, offline website data and site settings.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

Clearing your cache and website data is a great way to protect your internet privacy. If you run into issues with your browser, it can also be a good troubleshooting step to get things back up and running. Why do we suggest this step when you see ads?

- Your browser cache may retain information from a previous visit to a website with ads. If you delete the cache, they can no longer load.
- A website may have created cookies on your computer that interfere with ad blocking.
- Malware can change your browser settings to keep your ad blocker from working.

**So how do you do this with ABB?**

1. Open the Adblock Browser app.
2. Tap the **Android menu icon** and select **Settings**.
3. Tap **Privacy and security**.
4. Choose **Clear browsing data**.
5. Tap the *Time range* drop-down menu to select a timeframe.
6. From the *Basic* tab, the following items are selected by default. Tap a specific option if you do not want to clear its associated data.
    - Browsing history
    - Cookies, media licenses, and site data
    - Cached images and files
7. From the *Advanced* tab, choose to clear the following items: 
    - Browsing history (selected by default)
    - Cookies and site data (selected by default)
    - Media licenses
    - Cached images and files (selected by default)
    - Saved passwords
    - Autofill form data
    - Site settings
8. Tap **Clear data**.

![Clear browsing history in Adblock Browser](/src/img/gif/clear-data-abb.gif)
