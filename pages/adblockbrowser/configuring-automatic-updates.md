title=Configuring automatic updates
description=Choose when to allow updates for Adblock Browser.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

Choose when to allow updates for Adblock Browser filter lists:

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Scroll down to **Update on Wi-Fi only**.
5. Toggle the option on or off based on your preference.

When the option is toggled on, filter list updates will happen only when you are connected to a Wi-Fi network.

<aside class="alert info" markdown="1">
**Important note**: When the option is turned on, it may affect the ad blocking quality in times where there are important filter changes being made.
</aside>

![Configure automatic updates in Adblock Browser](/src/img/gif/wifi-setting-abb.gif)
