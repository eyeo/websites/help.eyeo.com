title=Removing a filter list
description=Remove a filter list that you previously added to Adblock Browser.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

You may want to remove a filter list to unblock certain items, or because the filter list is no longer relevant to your browsing habits.

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Tap **More blocking options**.
5. Tap the the **trash can icon** next to the filter lists’ URL.