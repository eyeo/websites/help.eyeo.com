title=Adjusting privacy settings
description=Adjust custom Adblock Browser privacy settings such as Do Not Track and safe browsing.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

In addition to ad blocking, Adblock Browser for Android features a number of privacy features that help you to remain safe while browsing the web.

1. Open the Adblock Browser app.
2. Tap the **Android menu icon** and select **Settings**.
3. Tap **Privacy & security**.
4. To turn off a feature that is turned on by default, clear the check box next to that feature. To turn on a feature, tap the check box next to that feature.

![Adjust privacy settings in Adblock Browser](/src/img/gif/privacy-settings-abb.gif)