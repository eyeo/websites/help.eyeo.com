title=Filter lists disappearing after Firefox update
description=Noticing ads after the latest Firefox Update? Filter lists disappearing? (What are Filter Lists?) If you are experiencing any of these symptoms continue reading this article!
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

Noticing ads after the latest Firefox Update? Filter lists disappearing? ([What are Filter Lists?](adblockplus/what-are-filter-lists)) If you are experiencing any of these symptoms continue reading this article!

Our team did some trials and with the help of our users' reports, have found the cause of the issue. After the latest Firefox update "always browse in private mode" seems to interfere with ABP functionality. The good news is that the fix couldn't be easier! Simply disable this in your browser's settings to resolve the issue.

Here is what you should do:

1. Click on your **Firefox Menu** 
2. Go to **Settings** <br /><img alt="" src="/dist/img/png/firefoxcrashsettings.png" />
3. Click on **Privacy and Security** <br /><img alt="" src="/dist/img/png/firefoxcrashps1.png" /><br />
Scroll down until the **History** section.
4. Disable the option **"Always use in Private Mode"** and make sure you do not have **"Never Remember History"** selected. Alternatively, you can simply select **"Clear History when Firefox closes."** <br /><img alt="" src="/dist/img/png/firefoxcrashps.png" /><br />Following these simple steps ensures that Adblock Plus runs smoothly on the latest Firefox version!