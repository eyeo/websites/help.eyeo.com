title=How to check your installed extensions
description=Having issues with a website? Not sure if Adblock Plus is causing the issue? Checking your browser settings helps with having an overview of all your installed or active extensions.
template=article
product_id=abp
category=Troubleshooting & Reporting


<section class="platform-chrome" markdown="1">
## Chrome

1. Click the settings icon on the far right of the browser
2. Choose **More Tools**
3. Click **Extensions**
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. Click the ![](/src/img/png/settings-menu-icon.png)icon in the far right corner of the browser
2. Choose **Add-ons**
</section>

<section class="platform-ios" markdown="1">
### Adblock Plus for iOS

1. Open the settings app from your home screen 
2. Choose **Safari**
3. Click on **Content Blockers**
</section>

<section class="platform-safari" markdown="1">
## Safari

1. Click **Safari** in the left corner of of the browser
2. Choose **Preferences**
3. Click on **Extensions **
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. Select **Tools** button
2. Select **Manage add-ons**.
3. Under **Show** select **All add-ons**.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. At the top corner of the browser, select **Settings and more** 
2. Choose **Extensions**
</section>

<section class="platform-opera" markdown="1">
## Opera

1. Click **View** in the toolbar.
2. Choose Show **Extensions**
<aside class="alert info" markdown="1">
**Tip**: You can also use this key shortcut (⌘ + Shift + E)
</aside>
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

1. Click the settings menu icon in the far left corner of the browser
2. Click **Add-ons**
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. Click the settings menu icon ![](/src/img/png/settings-menu-icon.png)
2. Click **Extensions**
</section>