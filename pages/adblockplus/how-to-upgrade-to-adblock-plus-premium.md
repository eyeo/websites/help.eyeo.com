title=What is Adblock Plus Premium? How do I upgrade?
description=Adblock Plus Premium takes your ad blocking and customization to the next level. It allows you to block annoying distractions like site notifications, floating videos, and newsletter pop-ups.
template=article
product_id=abp
category=Premium
hide_browser_selector=1

Adblock Plus Premium takes your ad blocking and customization to the next level. It allows you to block annoying distractions like site notifications, floating videos, and newsletter pop-ups. You can even ignore survey requests! Adblock Plus Premium is only available on desktop versions of Chrome, Firefox and Edge and is not available on mobile devices.

## How to Upgrade to Premium? 

To Upgrade to Premium, do the following:

1. Click on the Adblock Plus toolbar icon. If the icon is not on the browser toolbar, please make sure to pin it: [How to hide/show icon](adblockplus/hide-the-adblock-plus-icon)
2. Click on the **Upgrade** button showing in the right corner of the ABP menu. 
3. On the Adblock Plus options page, click on **Get Premium** in the column displaying the subcription option of your choice ie: monthly/yearly.
4. On the enrollment page, confirm your subscription type (monthly/yearly), select your payment method, complete your billing information. 
5. Click on **Upgrade to Premium**.

Your extension will be automatically upgraded and you can immediately try out the Adblock Plus Premium features.  If you encounter an activation error or to redeem on additional computers you use, please see this article.