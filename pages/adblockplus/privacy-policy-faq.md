title=Privacy Policy F.A.Q's
description=See what sort of data we collect and how we process it.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

Have questions about the data we collect and how it's processed?  Find the answers to frequently asked questions about our privacy policy here!

* [Does Adblock Plus collect any personal data?](https://adblockplus.org/en/faq-privacy#collect-personal-data)
* [Who collects my personal data when I use Adblock Plus?](https://adblockplus.org/en/faq-privacy#who-collects-data)
* [What kind of personal data does eyeo collect?](https://adblockplus.org/en/faq-privacy#what-kind-of-data)
* [Does eyeo track my browsing history?](https://adblockplus.org/en/faq-privacy#track-browser-history)
* [How does eyeo collect personal data?](https://adblockplus.org/en/faq-privacy#how-we-collect-data)
* [Why does eyeo collect personal data?](https://adblockplus.org/en/faq-privacy#why-we-collect-data)
* [Does eyeo sell data to third parties?](https://adblockplus.org/en/faq-privacy#selling-personal-data)
* [What are the legal standards or requirements associated with eyeo collecting personal data?](https://adblockplus.org/en/faq-privacy#legal-standards-requirements)
* [Does eyeo hash the data it collects?](https://adblockplus.org/en/faq-privacy#hashing-data)
* [How long does eyeo store personal data?](https://adblockplus.org/en/faq-privacy#data-expiry)
* [What rights do I have over personal data that eyeo has collected?](https://adblockplus.org/en/faq-privacy#personal-data-rights)
* [How can I exercise these rights?](https://adblockplus.org/en/faq-privacy#exercise-rights)
  
If you have more questions please feel free to contact our privacy team via [privacy@eyeo.com](mailto:privacy@eyeo.com).
  
[Privacy Policy (short version)](https://adblockplus.org/en/privacy#privacy-policy-short)
  
[Privacy Policy (long version)](https://adblockplus.org/en/privacy#privacy-policy-long)