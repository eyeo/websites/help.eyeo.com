title= Learn about Distraction Control
description=Adblock Plus Premium is happy to introduce our newest and highly requested feature: Distraction Control.
template=article
product_id=abp
category=Premium
hide_browser_selector=1

Adblock Plus Premium is happy to introduce our newest and highly requested feature: Distraction Control.

Some of our user`s most frequent complaints center around floating videos and site notifications (those little popups in the corner of your screen that are often confused with ads).  And we heard you! With distraction control enabled, you can get rid of those within seconds. The new feature also blocks other annoyances, such as survey requests and newsletter popups.

It is one step closer to putting users in full control of their online experience!

![](/src/img/png/Distraction_Control.png){: style="max-width: 454px" }

The new feature is available to Chrome, Edge, and Firefox browsers. To have access to Distraction Control, all you have to do is sign up to Adblock Plus Premium.
