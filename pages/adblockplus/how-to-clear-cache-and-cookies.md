title=How to clear cache and cookies
description=Learn how you can clear cache and cookies that interfere with the ad blocking functionality.
template=article
product_id=abp
category=Troubleshooting & Reporting

<section class="platform-chrome" markdown="1">
## Chrome

1. Click the 3 vertical dots in the upper right-hand corner of your browser.
2. From the Chrome menu, select **More tools**, then select **Clear browsing data**.
3. Select the time frame and which data you want to clear then click **Clear data**.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. Click the **Library **button in the upper right-hand corner of the browser, select **History**, then select **Clear Recent History.**
    ![Clearing browsing data in Firefox](/src/img/png/firefox-library.png)
2. Select the time frame and which data you want to clear then click **OK**.
</section>

<section class="platform-ios" markdown="1">
### Adblock Plus for iOS

1. Go to **Settings > Safari**
2. Tap **Clear History and Website Data**. 

Clearing your history, cookies, and browsing data from Safari won't change your AutoFill information.
</section>

<section class="platform-safari" markdown="1">
## Safari

1. In the Safari app ![](/src/img/png/ios-safari-app-icon.png) on your Mac, choose History > Clear History, then click the pop-up menu.
2. Choose how far back you want your browsing history cleared.
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. In Internet Explorer, select the **Favorites** button.

2. Select the **History** tab, and choose how you want to view your history by selecting a filter from the menu. To delete specific sites, right-click a site from any of these lists and then select **Delete**. Or, return to a page by selecting any site in the list.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. Click the **More** menu (**...**), then select **Settings.**
2. Go to **Privacy, Search and Services** and under **Clear browsing data**, click **Choose What to Clear.**
3. Select the time frame and which data you want to clear then click **Clear Now**.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. Click the "easy set up" icon in far right corner of the page
2. Scroll down to **Browsing Data** 
3. Choose **Clear**
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. Click ![](/src/img/png/settings-menu-icon.png) > **Advanced ** > **Clear history**.
2. Under **Delete the following**, selected the time range you want to delete.
3. Select the **Cached files** option.
4. Disable all the other options.
5.  Tap **Clear.**
</section>