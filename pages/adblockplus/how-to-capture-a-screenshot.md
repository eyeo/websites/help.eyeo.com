title=How to capture a screenshot
description=Here's how to capture a screenshot in order to identify the issues you're experiencing more easily.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

If you're troubleshooting an issue with support, it is often helpful to send us a screenshot so we can better identify the issue. 

[https://www.take-a-screenshot.org/](https://www.take-a-screenshot.org/) has instructions for [Mac](https://www.take-a-screenshot.org/mac.html), [Windows](https://www.take-a-screenshot.org/windows.html) and [Chrome OS](https://www.take-a-screenshot.org/chrome-os.html).

Using your mobile device? No problem! Here you can also find instructions for [Android](https://www.take-a-screenshot.org/android.html) and [iOS](https://www.take-a-screenshot.org/ios.html).