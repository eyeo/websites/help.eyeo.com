title=How to redeem your Adblock Plus Premium upgrade
description=If you have already paid for Adblock Plus Premium, follow the steps below to redeem your upgrade on your desktop browser extensions for Chrome, Firefox, and Edge
template=article
product_id=abp
category=Premium
hide_browser_selector=1

If you have already paid for Adblock Plus Premium, follow the steps below to redeem your upgrade on your desktop browser extensions for Chrome, Firefox, and Edge: 

1. Make sure you've installed the Adblock Plus extension: [How to check your installed extensions](adblockplus/how-to-check-your-installed-extensions)
2. Then go to the Adblock Plus Premium enrollment page.
3. At the bottom of the enrollment page, select **Adblock Plus Premium User?** <aside class="alert info" markdown="1">
**Tip**: You can also select this option if you've made a previous donation and would like to redeem the one-time complimentary 90 day upgrade.
</aside>
4. Enter the case-sensitive email address associated with the purchase or donation. 
5. When you receive the emailed code, enter it in the field provided. If you don't receive the code, be sure to check your spam/junk folder. 

## How to access your premium features

1. Click on the Adblock Plus toolbar icon. If you don't see the icon, make sure it is pinned: [How to hide/show icon](adblockplus/hide-the-adblock-plus-icon).
2. Click the gear symbol in the right corner of the Adblock Plus menu to access the settings page.
3. Click the **General** tab.

<aside class="alert info" markdown="1">
Premium features can be enabled under the General tab
</aside>

## I'm seeing an error message

In some cases you may encounter an error message during the upgrade process or when redeeming the premium upgrade. Below are errors you may see how to address them.

- If you are seeing an activation error after upgrading, [Remove and reinstall ABP](adblockplus/uninstall-adblock-plus) and the use the steps above to redeem the upgrade.
- If you see an error that the code is wrong, this is typically due to an old or invalid code being entered. Please delete all ABP containing codes from your inbox and try the steps above again to have a new code sent.
- If you see an error that your email is not recognized, please make sure you are entering the email exactly as it appears on your receipt.

Still having issues redeeming? please feel free to reach us via [support@adblockplus.org](mailto:support@adblockplus.org).