title=I get an error when I try to update Adblock Plus for the Safari app
description=If you're seeing errors when trying to update the Adblock Plus for Safari on macOS app, read these tips on what to do next.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

When trying to install the update to Adblock Plus for the Safari Mac app, are you getting one of these errors?

<aside class="alert info" markdown="1">
“Adblock Plus” cannot be updated because its extensions are in use by “Safari”. Click OK and quit the application.
</aside>

or,

<aside class="alert info" markdown="1">
"Adblock Plus" cannot be open during installation. Click Continue to quit "Adblock Plus" and begin the update. The application will open when the update is complete.
</aside>

As with all Mac apps, you need to quit Adblock Plus so App Store can install the update. What makes ABP a little more confusing than other apps is that it runs in the background of Safari. You can't see it, and you may not even know that it's open. 

### Solution 1: Click _Continue_

Try just clicking **Continue**. If all goes well, the app will close, update, and open again in the background.

### Solution 2: Click _Cancel_

If solution 1 did not work, try this:

1. Click **Cancel** to close the error message.
1. Quit Adblock Plus. (If you don't see the app window, force ABP to quit following the instructions at the bottom of this article. Then come back here and continue with Step 3.)
1. Quit Safari.
1. Open the Updates tab in the App Store and try to install the Adblock Plus update again.
1. If the issue still is not fixed, please try rebooting your device.

#### How to force an app to quit

 ![Screenshot with instructions on how to quit an app on macOS](/src/img/png/macos-quit-app.png)