title=Snippet Filters Tutorial
description=How to write snippet filters.
template=article
product_id=abp
category=Customization & Settings
hide_browser_selector=1


<style type="text/css">
  th, tr:nth-child(even) {
    background-color: #ececec;
  }
  td, th {
    padding-left: 8px;
    padding-right: 8px;
    border: 1px;
  }
  thead {
    font-weight: bold;
  }
</style>

This guide is aimed at filter authors and advanced users that want to make the most out of Adblock Plus. It's a companion to the [guide on writing filters](https://help.eyeo.com/en/adblockplus/how-to-write-filters) that covers snippet filters. While it's recommended that you be familiar with writing filters, you should be OK otherwise. You don't need a deep knowledge of JavaScript, but you need to be familiar with it, as well as with web technologies like HTML, CSS, and the DOM. If you really want, Mozilla has a great resource to [learn JavaScript, HTML and CSS](https://developer.mozilla.org/en-US/docs/Learn/JavaScript).

This guide doesn't cover how to write an actual snippet, i.e. the JavaScript code that is loaded by Adblock Plus into the browser when you execute a snippet filter. This requires modifying the extension, and it will be covered in an upcoming developer guide.

## Table of contents

- [What are snippet filters?](#about-snippet-filters)
- [Syntax](#syntax)
  - [Arguments](#arguments)
- [Examples](#examples)
- [Debugging](#debugging)
- [Snippets reference](#snippets-ref)

## <a name="about-snippet-filters"></a>What are snippet filters?

Snippet filters are a new type of filter introduced in Adblock Plus 3.3 that allows running specialized code snippets (written in JavaScript) on pages within a list of specified domains. These snippets are selected from an existing library available in Adblock Plus.

The snippet approach allows interfering with ad display code in a more efficient manner, and also allows locating elements to hide in a way that can't be expressed with CSS or the current advanced selectors.

For security reasons, snippet filters can only be added to a user's custom filters list or in the [*ABP Anti-Circumvention Filter List*](https://github.com/abp-filters/abp-filters-anti-cv), the latter being vetted and reviewed.

## <a name="syntax"></a>Syntax

The basic syntax of a snippet filter is as follows:

`domain#$#script`

- `domain` - A domain or a list of comma-separated domain names. See [example 2](#example-domains) and [how to write element hiding filters](https://help.eyeo.com/en/adblockplus/how-to-write-filters#elemhide_domains).

- `#$#` - This sequence marks the end of the domain list and the start of the actual snippet filter. `#$#` mean this is a snippet filter. There are other separator sequences for different kinds of filters, as explained in the [How to write filters](https://help.eyeo.com/en/adblockplus/how-to-write-filters#elemhide_basic) guide.

- `script` - A script that invokes one or more snippets.

The `script` has the following syntax:

`command arguments`

- `command` - The name of the snippet command to execute. Consult the [snippet commands reference](#snippets-refs) for a list of possible snippet commands.

- `arguments` - The snippet command arguments, separated by spaces. The [snippet commands reference](#snippets-refs) defines the arguments.

You can run several snippets in the same filter by separating them with a `;`. See [example 6](#example-multiple-snippets).

### <a name="arguments"></a>Arguments

Arguments are strings. They can be surrounded by single quotes (`'`) if they contain spaces. To have a single quote `'` as part of the argument, escape it like this: `\'` ; you can also escape spaces with `\ ` instead of quoting the argument. See [examples 3a](#example-quote), [3b](#example-quote-2) and [3c](#example-quote-3).

You can input Unicode characters by using the syntax `\uNNNN`, where NNNN is the Unicode codepoint. See [example 4](#example-unicode).

You can also input carriage return, line feed, and tabs using `\r`, `\n`, and `\t`, respectively. To have a `\` in an argument, including a regular expression, it must be escaped with `\`, i.e. doubled up. See [example 5](#example-lf).

## <a name="examples"></a>Examples

### Basic snippet filter syntax

These examples demonstrate the syntax of a snippet filter and how to pass arguments. While we mostly use the `log` snippet, these examples apply to other snippet functions as well.

#### <a name="example1"></a>Example 1

`example.com#$#log Hello`

This prints "Hello" in the web console on the domain `example.com` and its subdomains.

#### <a name="example-domains"></a>Example 2

`anotherexample.com,example.com#$#log Hello`

Same as example 1, but "Hello" also appears on the domain `anotherexample.com`. Like with other filter types, multiple domains should be separated by commas `,`.

This filter makes the filter in example 1 obsolete. Having both filters makes the message appear twice on `example.com`, as both would be run.

#### <a name="example-quote"></a>Example 3a

`example.net,example.com#$#log 'Hello from an example to another example'`

This is a different message from example 2. Because it contains spaces, the string must be surrounded with single quotes `'`.

#### <a name="example-quote-2"></a>Example 3b

`example.net,example.com#$#log 'Hello it\'s me again'`

This shows the escape sequence for having a single quote, using `\'`.

#### <a name="example-quote-3"></a>Example 3c

`example.net,example.com#$#log Hello\ no\ quotes`

You can also escape spaces instead of quoting the argument, using `\ `.


#### <a name="example-unicode"></a>Example 4

`emoji.example.com#$#log \u1F601`

On the domain `emoji.example.com`, the emoji 😀 is printed in the console. The filter will not be run on `example.com` or `www.example.com`. This shows how to use escape for Unicode symbols.

#### <a name="example-lf"></a>Example 5

`emoji.example.com#$#log '\u1F601\nand a new line'`

This is similar to example 4, except that the filter prints the emoji 😀 and then on a new line, prints `and a new line`. This shows how to use escape for Unicode symbols, as well as adding a new line.

#### <a name="example-multiple-snippets"></a>Example 6

Running two snippets:

`debug.example.com#$#debug; log 'This should be prefixed by DEBUG'`

First, we see `debug`. This is the snippet that enables debugging. It's just like any other snippet. Then we see `log`, whose output is modified when debugging is enabled. See the section on [debugging](#debugging) for more details.

`example.com#$#log 'Always log'; log 'And better twice than once'`

This is another example of logging two lines. Here the snippet command will be called twice.

## <a name="debugging"></a>Debugging

We saw a `debug` snippet in [Example 6](#example-multiple-snippets). Its use is simple: calling `debug` before a snippet turns on the debug flag. If the other snippets support it, they'll enable their debug mode. What a snippet does in debug mode is up to the snippet.

## <a name="snippets-ref"></a>Snippet commands reference


**log**

Since: Adblock Plus 3.3

Logs its arguments to the console.

This may be used for testing and debugging.


| Param | Type | Description |
| --- | --- | --- |
| [...args] | <code>\*</code> | The arguments to log. |






**hide-if-shadow-contains**

Since: Adblock Plus 3.3

Hides any HTML element or one of its ancestors matching a CSS selector if
the text content of the element's shadow contains a given string.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| search | <code>string</code> |  | The string to look for in every HTML element's   shadow. If the string begins and ends with a slash (`/`), the text in   between is treated as a regular expression. |
| selector | <code>string</code> | <code>&quot;*&quot;</code> | The CSS selector that an HTML element must match   for it to be hidden. |



**hide-if-contains**

Since: Adblock Plus 3.3

Hides any HTML element or one of its ancestors matching a CSS selector if
the text content of the element contains a given string.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| search | <code>string</code> |  | The string to look for in HTML elements. If the   string begins and ends with a slash (`/`), the text in between is treated   as a regular expression. |
| selector | <code>string</code> | <code>&quot;*&quot;</code> | The CSS selector that an HTML element must match   for it to be hidden. |
| [searchSelector] | <code>string</code> | <code>null</code> | The CSS selector that an HTML element   containing the given string must match. Defaults to the value of the   `selector` argument. |



**hide-if-contains-visible-text**

Since: Adblock Plus 3.6

Hides any HTML element matching a CSS selector if the visible text content
of the element contains a given string.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| search | <code>string</code> |  | The string to match to the visible text. Is considered   visible text that isn't hidden by CSS properties or other means.   If the string begins and ends with a slash (`/`), the text in between is   treated as a regular expression. |
| selector | <code>string</code> |  | The CSS selector that an HTML element must match   for it to be hidden. |
| [searchSelector] | <code>string</code> | <code>null</code> | The CSS selector that an HTML element   containing the given string must match. Defaults to the value of the   `selector` argument. |



**hide-if-contains-and-matches-style**

Since: Adblock Plus 3.3.2

Hides any HTML element or one of its ancestors matching a CSS selector if
the text content of the element contains a given string and, optionally, if
the element's computed style contains a given string.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| search | <code>string</code> |  | The string to look for in HTML elements. If the   string begins and ends with a slash (`/`), the text in between is treated   as a regular expression. |
| selector | <code>string</code> | <code>&quot;*&quot;</code> | The CSS selector that an HTML element must match   for it to be hidden. |
| [searchSelector] | <code>string</code> | <code>null</code> | The CSS selector that an HTML element   containing the given string must match. Defaults to the value of the   `selector` argument. |
| [style] | <code>string</code> | <code>null</code> | The string that the computed style of an HTML   element matching `selector` must contain. If the string begins and ends   with a slash (`/`), the text in between is treated as a regular   expression. |
| [searchStyle] | <code>string</code> | <code>null</code> | The string that the computed style of an HTML   element matching `searchSelector` must contain. If the string begins and   ends with a slash (`/`), the text in between is treated as a regular   expression. |



**hide-if-has-and-matches-style**

Since: Adblock Plus 3.4.2

Hides any HTML element or one of its ancestors matching a CSS selector if a
descendant of the element matches a given CSS selector and, optionally, if
the element's computed style contains a given string.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| search | <code>string</code> |  | The CSS selector against which to match the   descendants of HTML elements. |
| selector | <code>string</code> | <code>&quot;*&quot;</code> | The CSS selector that an HTML element must match   for it to be hidden. |
| [searchSelector] | <code>string</code> | <code>null</code> | The CSS selector that an HTML element   containing the specified descendants must match. Defaults to the value of   the `selector` argument. |
| [style] | <code>string</code> | <code>null</code> | The string that the computed style of an HTML   element matching `selector` must contain. If the string begins and ends   with a slash (`/`), the text in between is treated as a regular   expression. |
| [searchStyle] | <code>string</code> | <code>null</code> | The string that the computed style of an HTML   element matching `searchSelector` must contain. If the string begins and   ends with a slash (`/`), the text in between is treated as a regular   expression. |



**hide-if-contains-image**

Since: Adblock Plus 3.4.2

Hides any HTML element or one of its ancestors matching a CSS selector if
the background image of the element matches a given pattern.


| Param | Type | Description |
| --- | --- | --- |
| search | <code>string</code> | The pattern to look for in the background images of   HTML elements. This must be the hexadecimal representation of the image   data for which to look. If the string begins and ends with a slash (`/`),   the text in between is treated as a regular expression. |
| selector | <code>string</code> | The CSS selector that an HTML element must match   for it to be hidden. |
| [searchSelector] | <code>string</code> | The CSS selector that an HTML element   containing the given pattern must match. Defaults to the value of the   `selector` argument. |






**abort-on-property-read**

Since: Adblock Plus 3.4.1

Patches a property on the window object to abort execution when the
property is read.

No error is printed to the console.

The idea originates from
[uBlock Origin](https://github.com/uBlockOrigin/uAssets/blob/80b195436f8f8d78ba713237bfc268ecfc9d9d2b/filters/resources.txt#L1703).


| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | The name of the property. |



**abort-on-property-write**

Since: Adblock Plus 3.4.3

Patches a property on the window object to abort execution when the
property is written.

No error is printed to the console.

The idea originates from
[uBlock Origin](https://github.com/uBlockOrigin/uAssets/blob/80b195436f8f8d78ba713237bfc268ecfc9d9d2b/filters/resources.txt#L1671).


| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | The name of the property. |



**abort-current-inline-script**

Since: Adblock Plus 3.4.3

Aborts the execution of an inline script.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| api | <code>string</code> |  | API function or property name to anchor on. |
| [search] | <code>string</code> | <code>null</code> | If specified, only scripts containing the given   string are prevented from executing. If the string begins and ends with a   slash (`/`), the text in between is treated as a regular expression. |



**json-prune**

Since: Adblock Plus 3.9.0

Traps calls to JSON.parse, and if the result of the parsing is an Object, it
will remove specified properties from the result before returning to the
caller.

The idea originates from
[uBlock Origin](https://github.com/gorhill/uBlock/commit/2fd86a66).


| Param | Type | Description |
| --- | --- | --- |
| rawPrunePaths | <code>string</code> | A list of space-separated properties to remove. |
| [rawNeedlePaths] | <code>string</code> | A list of space-separated properties which   must be all present for the pruning to occur. |



**freeze-element**

Since: Adblock Plus 3.10

Freezes a DOM element so it prevents adding new nodes inside it.


| Param | Type | Description |
| --- | --- | --- |
| selector | <code>string</code> | The CSS selector for the parent element that   we want to freeze |
| [options] | <code>string</code> | A single parameter for snippet's options.   A string containing all the options we want to pass, each of them   separated by a plus character (`+`). Empty single quotes if none (`''`).   Available options:   **subtree** (if we want to freeze all the element's children as well);   **abort** (throw an error every time an child element gets added); |
| [...exceptions] | <code>string</code> | An array of regex/selectors used to specify   the nodes we don't want to prevent being added.   Each array item can be:   **selector** (targeting Element nodes);   **regex** (targeting Text nodes, identified by slash); |



**override-property-read**

Since: Adblock Plus 3.9.4

Overrides a property's value on the window object with a set of
available properties.

Possible values to override the property with:
  undefined
  false
  true
  null
  noopFunc   - function with empty body
  trueFunc   - function returning true
  falseFunc  - function returning false
  ''         - empty string
  positive decimal integer, no sign, with maximum value of 0x7FFF

The idea originates from
[uBlock Origin](https://github.com/uBlockOrigin/uAssets/blob/80b195436f8f8d78ba713237bfc268ecfc9d9d2b/filters/resources.txt#L2105).


| Param | Type | Description |
| --- | --- | --- |
| property | <code>string</code> | The name of the property. |
| value | <code>string</code> | The value to override the property with. |



**abort-on-iframe-property-read**

Since: Adblock Plus 3.10.1

Patches a list of properties on the iframes' window object to abort execution
when the property is read.

No error is printed to the console.


| Param | Type | Description |
| --- | --- | --- |
| ...properties | <code>string</code> | The list with the properties. |



**abort-on-iframe-property-write**

Since: Adblock Plus 3.10.1

Patches a list of properties on the iframes' window object to abort execution
when the property is written.

No error is printed to the console.


| Param | Type | Description |
| --- | --- | --- |
| ...properties | <code>string</code> | The list with the properties. |



**strip-fetch-query-parameter**

Since: Adblock Plus 3.5.1

Strips a query string parameter from `fetch()` calls.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The name of the parameter. |
| [urlPattern] | <code>string</code> | <code>null</code> | An optional pattern that the URL must match. |



**ml-hide-if-graph-matches**

Since: Adblock Plus 3.8

Hides any HTML element if its structure (graph) is classified as an ad
by a built-in machine learning model.


| Param | Type | Description |
| --- | --- | --- |
| selector | <code>string</code> | A selector that produces a list of targets to classify. |
| tagName | <code>string</code> | An HTML tag name to filter mutations. |






**hide-if-labelled-by**

Since: Adblock Plus 3.9

Hides any HTML element that uses an `aria-labelledby`, or one of its
ancestors, if the related aria element contains the searched text.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| search | <code>string</code> |  | The string to look for in HTML elements. If the   string begins and ends with a slash (`/`), the text in between is treated   as a regular expression. |
| selector | <code>string</code> |  | The CSS selector of an HTML element that uses as   `aria-labelledby` attribute. |
| [searchSelector] | <code>string</code> | <code>null</code> | The CSS selector of an ancestor of the   HTML element that uses as `aria-labelledby` attribute. Defaults to the   value of the `selector` argument. |



**hide-if-matches-xpath**

Since: Adblock Plus 3.9.0

Hide a specific element through a XPath 1.0 query string.
See [Getting started with XPath filters](./xpath-filters) to know more.


| Param | Type | Description |
| --- | --- | --- |
| query | <code>string</code> | The XPath query that targets the element to hide. |



**debug**

Since: Adblock Plus 3.8

Enables debug mode.



**profile**

Since: Adblock Plus 3.9

Enables profile mode.



**trace**

Since: Adblock Plus 3.3

Similar to `log`, but does the logging in the context of the document rather
than the content script.

This may be used for testing and debugging, especially to verify that the
injection of snippets into the document is working without any errors.


| Param | Type | Description |
| --- | --- | --- |
| [...args] | <code>\*</code> | The arguments to log. |


