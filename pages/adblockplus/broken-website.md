title=Adblock Plus breaks the websites I visit
description=In rare instances, Adblock Plus may prevent some websites from functioning correctly. This is typically not a bug but rather a problem with the filters (or filter subscription).
template=article
product_id=abp
category=Troubleshooting & Reporting
popular=true

In rare instances, Adblock Plus may prevent some websites from functioning correctly. This is typically not a bug but rather a problem with the filters (or filter subscription).

## Temporarily turn off Adblock Plus on a website {: #turn-off-abp }

A filter list may be directing Adblock Plus to block something that should be allowed. To verify that the issue lies within the filter list, temporarily turn off Adblock Plus on the website that you are experiencing the issue. If the issue is fixed after temporarily disabling Adblock Plus, you can be certain that the problem is within the filters.

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
Adblock Plus is now turned off on the website you are on.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and then click the toggle button.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
Adblock Plus is now turned off on the website you are on.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
Adblock Plus is now turned off on the website you are on.
</section>

<section class="platform-msie" markdown="1">
### Internet Explorer

1. From the status bar located at the bottom of the browser, click the **Adblock Plus** icon and select **Disable on [website]**.
<aside class="alert info" markdown="1">
**Tip**: If you do not see the Adblock Plus icon, right-click the tab bar (the empty area near the tabs) and select **Status bar**.
</aside>
Adblock Plus is now turned off on the website you are on.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
Adblock Plus is now turned off on the website you are on.
</section>

<section class="platform-safari" markdown="1">
### Safari

1. From the Safari toolbar, click the **Adblock Plus** icon and select **Enabled on this site**.
<br>Adblock Plus is now turned off on the website you are on.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select the toggle button.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
Adblock Plus is now turned off on the website you are on.
</section>

## Update the filter list(s) {: #update-filter-lists }

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-ios" markdown="1">
### Adblock Plus for iOS

1. Open the Adblock Plus for iOS app.
2. Tap the **Tools** icon.
3. Tap **Update Filter lists**.
4. Wait 10-20 seconds for the lists to update.
<br>The update is successful if the current date and time appears below *Update filter lists*.
5. Close the app.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-samsungBrowser" markdown="1">
### Adblock Plus for Samsung Internet

1. Open the Adblock Plus for Samsung Internet app.
2. Tap **Configure your filter lists**.
3. Clear the check box next to the filter you want to update.
4. Tap the back button to refresh.
5. Tap **Configure your filter lists**.
6. Tap the check box next to the filter you want to update.
7. Close the app.
8. Reopen the app to verify that the filter list has been updated.
<br>The update is successful if the current date and time appears below the list.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

## Report bad filters {: #report-bad-filters }

If updating the filter list(s) does not work, please contact our Support team at support@adblockplus.org.
