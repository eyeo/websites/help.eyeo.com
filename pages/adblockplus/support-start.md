title=Need to contact support? Look here first! 
description=Here's some useful information we'll need from you to best assist with any technical issues you're experiencing.
template=article
product_id=abp
category=Troubleshooting & Reporting
popular=true
hide_browser_selector=true

Whether you are seeing ads with ABP enabled or are experiencing other issues with the extension, you are more than welcome to contact our team via email ([support@adblockplus.org](mailto:support@adblockplus.org)). 

Providing the details below helps us to best assist you. 

### 1. Which browser are you using?

ABP works on _Chrome_, _Firefox_, _Internet Explorer_, _Microsoft Edge_, _Opera_, and _Yandex Browser_. In addition, we also have _Adblock Browser_, _ABP for Samsung Internet and iOS_ for mobile.

Knowing the browser you are using (and whether it is mobile or desktop) is the first step in reproducing and resolving the issue.

### 2. Which filter lists do you have enabled?

Filter lists are the flagship of ad blockers. They are what determine what should be blocked and hidden on web pages you visit. 

The following filter lists are pre-installed with Adblock Plus:

* Acceptable Ads
* EasyList (+ bundled language filter list - depending on your browser’s language setting)
* ABP Anti-circumvention filter list

You can also subscribe to other filter lists and even write your own! You can read more about it [here](adblockplus/add-a-filter-list).

To check your filter lists, click on your _ABP icon > gear icon > advanced_ and scroll down to filter lists.

### 3. What is your ABP/browser version?

Having the most recent version of ABP is imperative for the best functionality of the extension. 

[Check your Adblock Plus version](adblockplus/check-abp-version)

### 4. Do you have Acceptable Ads enabled?

You can check if you have AA enabled by clicking on the _ABP icon > gear icon > Allow Acceptable Ads_.

You can read more about them here: [What are Acceptable Ads?](adblockplus/what-are-acceptable-ads)

### 5. In which URL are you experiencing the issue?

URL is the page where you are experiencing the issue. In order to fix the problem, our team needs to know exactly where it is happening.

### 6. Screenshots.

Screenshots are important for our team to visualize the issue and assess if it is ABP related.

[How to capture a screenshot](adblockplus/how-to-capture-a-screenshot)