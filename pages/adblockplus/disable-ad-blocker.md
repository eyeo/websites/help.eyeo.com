title=A site asks me to disable ABP. What to do?
description=Here's what to do when a website asks you to turn off your ad blocker.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

One of the most popular questions our team receives is if there is any workaround for when this popup appears:

 ![Screenshot of anti ad blocker wall](/src/img/png/anti-adblocker-wall.png)
 
Websites are in their right to request users to disable ad blocker. And while we do believe anti ad blocking walls are an intrusive practice, we also understand the reasons publishers ask users to allow ads to see their content. 
 
Most importantly, although it's technically possible to block those notifications, in many cases it is illegal to stop them.
 
## What to do when they appear?

When seeing one of these popups, you can either [disable ABP for the specific page](adblockplus/turn-abp-on-off-manually), or you can add the website to the allowlist.

## What if the notification still shows up after disabling ABP?

If you have followed the steps above and the popup is still appearing on your screen, it is most likely something else causing it. You can read more in this article here: [Why am I still asked to turn off my ad blocker after allowlisting?](adblockplus/asked-to-turn-off-adblocker-after-allowlisting)