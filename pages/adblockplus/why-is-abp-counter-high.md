title=Why is my ABP counter so high?
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

The numbers on the ABP counter do not account only for the number of ads blocked. There are other factors that can also interfere with what is being presented up there.

![Screenshot of the Adblock Plus menu](/src/img/png/abp-popup-menu.png)

Many users question why the numbers are sometimes too high, or why there are any numbers at all if there are no ads on the page.

Below you will see a list of what could possibly be causing the issue! 

### Too many filter lists

Having too many filters means that more elements in the page are being blocked, reflecting on the ABP counter.

### EasyPrivacy enabled

EasyPrivacy blocks also tracking server requests, which means that the numbers showing in the counter are not only ads, but also tracking requests.

### Fanboy's Social Blocking List enabled

This particular list blocks all social media icons on the page. This also is accounted for on the ABP counter.

### ABP Filters enabled

This specific filter list is one of the default lists by ABP. They are used for countering ad blocking circumvention attempts and it has some elements that can lead to the number increasing.

### Custom Filters

If the user has added broad custom filters or installed a low-quality custom filter list.

### Unintentional blocking

Some web resources may be blocked because they look like ads but aren't. Also, there can always be bugs in the extension or in a recommended filter list.

### Long page views or repeating requests

If the user stays on the same web page for a long time, this might lead to many requests being blocked over this period of time (especially if the same request is made repeatedly, such as for tracking).

### Improper handling of blocked requests by websites

Usually, websites either ignore when a request fails or they implement some form of graceful degradation (e.g. falling back to an installed system font when a font file failed to download). However, some websites simply keep trying. And if they don't put a condition in place to stop retrying, the counter can increase infinitely.

It is a vicious cycle: the website tries to download, we block it. It tries to download it again, we block it again, and so on.

### Malware

Malware and other extensions could also inject ads and other kinds of content into websites, which we might block. This could cause the counter to go up even on websites that aren't expected to have any ads on them.