title=Pop-up ads in the lower right-hand corner of your browser
description=See how you can dismiss the pop-up ads in the lower right-hand corner of your browser so you won't see these browser notifications.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

![Screenshot of popup ads notifications in Chrome](/src/img/png/browser-popup-ads.png)

While these may look like ads they're actually site notifications, which means the only way to get rid of them is to change your Chrome settings. Here's how to change your settings in Chrome so that you no longer see those annoying Windows pop-ups:

## Disable Site Notifications in Chrome

1. Click the Chrome menu (the three vertical dots in the upper right corner of the Chrome window) and select **Settings**
1. Under "Privacy and security" click **Site Settings**
1. Under "Permissions" click **Notifications**
1. Turn off notifications from any site you no longer want to see notifications from

If that doesn't stop the pop-ups, the ads might be adware. 

[What is adware and what can I do about it?](adblockplus/adware)