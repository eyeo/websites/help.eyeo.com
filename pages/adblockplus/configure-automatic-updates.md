title=Configure automatic updates
description=Choose when to allow updates for Adblock Plus for Samsung Internet.
template=article
product_id=abp
category=Customization & Settings

Currently, it is only possible to configure automatic updates for filter lists via Wi-Fi or mobile data for Adblock Plus for Samsung Internet.

1. Open the Adblock Plus for Samsung Internet app.
2. Tap **Update ad-filter lists**
<br>After tapping on Automatic Updates select one of the following options:
    - **On Wi-Fi only** (activated by default) - Updates the filter lists only when you are connected to Wi-Fi.
    - **Always** - Updates the filter lists at any time, using either Wi-Fi or mobile data.
3. Close the app.
