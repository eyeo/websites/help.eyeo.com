title=Get started with Adblock Plus Premium
description=Adblock Plus Premium takes your ad blocking and customization to the next level. It allows you to block annoying distractions like site notifications, floating videos, and newsletter pop-ups.
template=article
product_id=abp
category=Premium
hide_browser_selector=1

**Step 1 of 2**{: style="color:gray" }

## Pin the extension to access ABP anytime {: style="margin-top: 1rem" }

From the Chrome toolbar, click on the **puzzle icon** to view a list of all your installed extensions.

![](/src/img/png/chrome_extension_icon.png){: style="max-width: 454px" }

Click on the **pin icon** next to the Adblock Plus extension. The pin icon will turn blue. 

![](/src/img/png/chrome_pin_icon.png){: style="max-width: 454px" }

The **Adblock Plus icon** now appears on the toolbar to the right.

![](/src/img/png/chrome_with_extension.png){: style="max-width: 454px" }

**Step 2 of 2**{: style="color:gray" }

## Turn On Your Premium Features {: style="margin-top: 1rem" }

From the Chrome toolbar, click on the **Adblock Plus** icon to show the menu.

![](/src/img/png/Frame_7.png){: style="max-width: 454px" }

Click the **gear symbol** in the right corner of the Adblock Plus menu to visit the settings page.

![](/src/img/png/gear.png){: style="max-width: 454px" }

Under the general tab you will see **Premium Features** you can turn on

![](/src/img/png/premium_option_in_general_tab.png){: style="max-width: 454px" }