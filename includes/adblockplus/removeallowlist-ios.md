1. Open the Adblock Plus for iOS app.
2. Tap the **Tools** icon.
3. Tap **Allowlisted Websites**.
4. If a website that you do not want to view ads on is listed below *Your Allowlist*, tap the **Trash** icon next to the website.
5. Close the app.
